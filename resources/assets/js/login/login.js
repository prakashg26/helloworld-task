
/**
 * On click of Login button
 *
 */
$('.login').click(function() {
	toastr.remove();

	let email		= $('#email').val(),
	    password	= $('#password').val();

	let validated	= validateFields(
						email,
						password
					);

	if (validated)
		redirect('/convert-currency');

	return false;
});

/**
 * To validate login input fields
 *
 * @param email
 * @param password
 * @returns {bool}
 */
function validateFields(email, password) {
	if (empty(email) && empty(password)) {
		warning('All input fields are mandatory');
		return false;
	}

	if (isValidEmail(email)) {
		let checkPassword	= isValidPassword(password); //Can use base64.js to encode password like base64.encode(password)

		if (checkPassword)
			return true;

		warning('Password is wrong');
		return false;
	
	}
	warning('Email ID is invalid');
	return false;

}
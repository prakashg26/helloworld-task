/**
 * On click of Reset button
 *
 */
$('#reset').click(function() {
	$('.currency-convertion').val('');
});

/**
 * On click of Add button
 *
 */
$('#add').click(function() {
	$('#show_currencies').addClass('hide');
	let params = {};

	if (!empty($('#currency').val() && $('#currency_code').val() && $('#convertion_rate').val())) {

		params.currency 		= $('#currency').val();
		params.currencyCode 	= $('#currency_code').val();
		params.convertionRate 	= $('#convertion_rate').val();
		
		addCurrency(
			params
		);

		$('#reset').trigger('click');
	} else {
		warning('Please fill all input fields.');
	}
});

/**
 * Show records
 *
 */
$('#show').click(function() {
	showLists();
});

/**
 * Render Lists
 *
 * @param currencies
 */
function renderCurrencies(currencies) {
	$('#show_currencies').removeClass('hide');

	let row	= '';
	$.each(currencies, function(index, element) {
		row	+= '<tr>';
		let col	= '';
		$.each(element, function(key, value) {
			col	+= '<td>'+value+'</td>';
		});
		row += col + '</tr>';
	});
	$('#show_currencies').append(row);
}
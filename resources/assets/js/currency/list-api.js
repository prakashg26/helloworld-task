/**
 * Ajax call for getting currency lists
 *
 */
function getCurrencyLists() {
	$.ajax({
        url         : api + '/lists',
        type        : 'GET',
        headers     : helloworld.getJsonHeaders(),
        success     : function(result) {
        	success('Listed Successfully');
        	showCurrencyLists(result.data.lists);
        },
        error 		: function(errorResponse) {
            info('Something went wrong!!!');
        }
    });
}

/**
 * Ajax call for converting currency
 *
 * @param {from_currency}
 * @param {to_currency}
 * @param {amount}
 */
function currencyConverter(from_currency, to_currency, amount) {
	$.ajax({
        url         : api + '/converter/'+from_currency+'/'+to_currency+'/'+amount,
        type        : 'GET',
        headers     : helloworld.getJsonHeaders(),
        success     : function(result) {
        	showConvertedCurrency(result.data.converted);
        },
        error 		: function(errorResponse) {
            info('Something went wrong!!!');
        }
    });
}

/**
 * Ajax call for add currency
 *
 * @param {params}
 */
function addCurrency(params) {
	$.ajax({
        url         : api + '/currency',
        type        : 'POST',
        headers     : helloworld.getJsonHeaders(),
        data		: JSON.stringify(params),
        success     : function(result) {
        	success('Currency has been added successfully');
        },
        error 		: function(errorResponse) {
            info('Something went wrong!!!');
        }
    });
}

/**
 * Ajax call for show currency lists
 *
 */
function showLists() {
	$.ajax({
        url         : api + '/currencies',
        type        : 'GET',
        headers     : helloworld.getJsonHeaders(),
        success     : function(result) {
        	renderCurrencies(result.data.currencies);
        },
        error 		: function(errorResponse) {
            info('Something went wrong!!!');
        }
    });
}
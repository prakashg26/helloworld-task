
$(function () {
	/**
	 * Get currency lists
	 *
	 */
	getCurrencyLists();	
	
});

/**
 * To show lists of currency
 *
 * @param lists
 */
function showCurrencyLists(lists) {
	$.each(lists, function(index, element) {
		$('#from_currency, #to_currency').append('<option id="'+element+'" value="'+element+'">'+index+'</option>');
	});
}

/**
 * On click convert button
 *
 */
$('#convert').click(function() {
	toastr.remove();
	let fromCurrency 	= $('#from_currency').val(),
		toCurrency 		= $('#to_currency').val(),
		amount			= $('#amount').val();

	if (!empty(amount) && !(fromCurrency == toCurrency)) {
		currencyConverter(
			fromCurrency,
			toCurrency,
			amount
		);
	} else {
		warning('Please give valid inputs.');
	}

});

/**
 * To show converted currency
 *
 * @param {converted_currency}
 */
function showConvertedCurrency(converted_currency) {
	swal(
        'Converted Successfully',
        'Converted Amount Is '+converted_currency,
        'success'
    );
}




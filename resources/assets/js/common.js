// Global Variables
let api             = window.location.origin + '/api/v1';
let baseUrl         = window.location.origin;

let	helloworld	= {
    getJsonHeaders: function () {
        return {
            'X-CSRF-TOKEN': $('meta[name="token"]').attr('content'),
            "Content-Type":"application/json",
            "Accept":"application/json",
        };
    }
}
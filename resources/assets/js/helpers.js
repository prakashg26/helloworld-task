/**
 * Verify for emptiness.
 *
 * @param item
 * @returns {boolean}
 */
function empty(item) {
    return item == null || item == undefined || item.length == 0 || item == ' '
}

/**
 * Check if the email is a valid format.
 *
 * @param boolean
 */
function isValidEmail(email) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

/**
 * Warning for toaster message.
 *
 * @param message
 */
function warning(message, options) {
    if (empty(options)) {
        options = {
            "newestOnTop": true,
            "closeButton": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "timeOut": "8000"
        }
    }

    toastr.warning(message, '', options);
}

/**
 * Warning for toaster message.
 *
 * @param message
 */
function success(message, options) {
    if (empty(options)) {
        options = {
            "newestOnTop": true,
            "closeButton": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "timeOut": "8000"
        }
    }

    toastr.success(message, '', options);
}

/**
 * Info Message
 *
 * @param message
 * @param options
 */
function info(message, options) {
    if (empty(options)) {
        options = {
            "newestOnTop": true,
            "closeButton": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "timeOut": "8000"
        }
    }

    toastr.info(message, '', options);
}

/**
 * Redirect Function
 *
 * @param url
 * @param params
 */
function redirect(url, params = null) {
    let query = '';

    if (!empty(params)) {
        query = '?' + get_query_string(params);
    }

    window.location.href = window.location.origin + url + query;
}

// Prototype for
String.prototype.ucFirst    = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

/**
 * Validate Password
 *
 * @param password
 * @returns {bool}
 */
function isValidPassword(password) {
	return password == 'magic' ? true : false; //Can use base64.js to decode password like base64.decode(password)
}
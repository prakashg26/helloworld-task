<meta id="token" name="token"  content="{{ csrf_token() }}"/>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<link rel="stylesheet" href="{!! asset('/css/toastr.css') !!}">
<link rel="stylesheet" href="{!! asset('/css/sweetalert2.min.css') !!}">
<style>
	.hide {
		display: none;
	}
</style>
<div class="col-sm-12" style="text-align: center;">
	<label>Currency</label>
	<input type="text" id="currency" value="" class="currency-convertion"/>

	<label>Currency Code</label>
	<input type="text" id="currency_code" value="" class="currency-convertion"/>

	<label>Convertion Rate</label>
	<input type="text" id="convertion_rate" value="" class="currency-convertion"/>

	<button id="add">Add</button>
	<button id="reset">Reset</button>
	<button id="show">Show Records</button>
	<a href="/convert-currency">Convert</a>
</div>

<div class="col-sm-12" style="text-align: center;margin-top: 2pc;">
	<table style="width:100%" class="hide" id="show_currencies">
	  <tr>
	    <th>Currency</th>
	    <th>Currency Code</th> 
	    <th>Convertion Rate</th>
	  </tr>
	</table>
</div>

<!-- @here, We should use gulp / grunt to make compressive. I am not using anything because considering in doing code task-->
<script src="{!! asset('/resources/assets/js/currency/manage.js') !!}"></script>
<script src="{!! asset('/resources/assets/js/currency/list-api.js') !!}"></script>
<script src="{!! asset('/resources/assets/js/common.js') !!}"></script>
<script src="{!! asset('/resources/assets/js/helpers.js') !!}"></script>
<script src="{!! asset('/js/sweetalert2.min.js') !!}"></script>
<script src="{!! asset('/js/toastr.js') !!}"></script>
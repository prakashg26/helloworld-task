<link rel="stylesheet" href="{!! asset('/resources/assets/css/login.css') !!}">
<link rel="stylesheet" href="{!! asset('/css/toastr.css') !!}">
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<div class="container">
    <div class="login-container">
        <div id="output"></div>
        <div class="avatar"></div>
        <div class="form-box">
            <form action="" method="">
                <input name="user" type="text" placeholder="email" id="email" value="">
                <input type="password" placeholder="password" id="password" value="">
                <button class="btn btn-info btn-block login" type="submit">Login</button>
                <a class="register" href="https://php.net">Register</button>
            </form>
        </div>
    </div>    
</div>
<script src="{!! asset('/resources/assets/js/login/login.js') !!}"></script>
<script src="{!! asset('/resources/assets/js/helpers.js') !!}"></script>
<script src="{!! asset('/js/toastr.js') !!}"></script>

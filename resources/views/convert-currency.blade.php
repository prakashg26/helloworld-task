<meta id="token" name="token"  content="{{ csrf_token() }}"/>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<link rel="stylesheet" href="{!! asset('/css/toastr.css') !!}">
<link rel="stylesheet" href="{!! asset('/css/sweetalert2.min.css') !!}">
<div class="col-sm-12" style="text-align: center;">
	<label>From</label>
	<select id="from_currency">
		
	</select>

	<label>Amount</label>
	<input type="text" name="amount" value="" id="amount">

	<label>To</label>
	<select id="to_currency">
		
	</select>

	<button id="convert">Convert</button>
	<a href="/manage">Manage</button>

</div>

<!-- @here, We should use gulp / grunt to make compressive. I am not using anything because considering in doing code task-->
<script src="{!! asset('/resources/assets/js/currency/convert.js') !!}"></script>
<script src="{!! asset('/resources/assets/js/currency/list-api.js') !!}"></script>
<script src="{!! asset('/resources/assets/js/common.js') !!}"></script>
<script src="{!! asset('/resources/assets/js/helpers.js') !!}"></script>
<script src="{!! asset('/js/sweetalert2.min.js') !!}"></script>
<script src="{!! asset('/js/toastr.js') !!}"></script>
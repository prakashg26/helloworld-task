<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Currency;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // For this task i am doing with static json list, we can have api too to get the lists
        $lists = file_get_contents('C:/xampp/htdocs/Assignment/HelloWorld/app/Http/currency.json');

        return response()->jsend([
            'lists'    => json_decode($lists)
        ], trans('api.success'), 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currency       = $request->currency;
        $currencyCode   = $request->currencyCode;
        $convertionRate = $request->convertionRate;

        // Actually we should have form request validation class for validating user input parameters. I am not doing for now by considering in doing task
        $store  = Currency::store(
                    $currency,
                    $currencyCode,
                    $convertionRate
                );

        return response()->jsend([
            'action'    => 'success'
        ], trans('api.success'), 200);        
    }

    /**
     * Display the list of resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $currencies   = Currency::show();

        return response()->jsend([
            'currencies'    => $currencies
        ], trans('api.success'), 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * convert the resource.
     *
     * @param $from_currency
     * @param $to_currency
     * @param $amount
     * @return \Illuminate\Http\Response
     */
    public function convert($from_currency, $to_currency, $amount)
    {
        $fromCurrency   = urlencode($from_currency);
        $toCurrency     = urlencode($to_currency);
        $amount         = $amount;

        // I am reading from this API -> Found from stack overflow
        $get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$fromCurrency&to=$toCurrency");

        // After printing, i got to understand to split the various
        $get = explode("<span class=bld>",$get);
        $get = explode("</span>",$get[1]);

        // Atlast by doing this, we can get the converted currency for the selection
        $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);
        
        return response()->jsend([
            'converted' => $converted_currency
        ], trans('api.success'), 201);
    }
}

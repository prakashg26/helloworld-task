<?php

Route::group([
    'prefix'        => 'api/v1',
], function () {
	// Getting Currency Lists
	Route::get('/lists', 'CurrencyController@index');

	// Currency convertion
	Route::get('/converter/{from}/{to}/{amount}', 'CurrencyController@convert');

	// Adding currency
	Route::post('/currency', 'CurrencyController@store');

	// Get Currency Lists
	Route::get('/currencies', 'CurrencyController@show');
});
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Api Routes
require_once 'api.php';

Route::get('/', function () {
    return view('login');
});

Route::get('/convert-currency', function () {
	return view('convert-currency');
});

Route::get('/manage', function () {
	return view('manage-currency');
});
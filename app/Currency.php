<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
	/**
     * @var string
     */
    protected $table    = 'tbl_currency_details';

    /**
     * @var bool
     */
    public $timestamps  = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'rate',
        'uuid'
    ];

    /**
     * Store currency.
     *
     * @param $currency
     * @param $currencyCode
     * @param $currencyRate
     * @return mixed
     */
    static function store($currency, $currencyCode, $currencyRate)
    {
        DB::table('tbl_currency_details')
        	->insert(['uuid'=>substr( md5(rand()), 0, 30), 'name'=>$currency, 'code'=>$currencyCode, 'rate'=>$currencyRate, 'status'=>1]);
    }

    /**
     * Show currency.
     *
     * @return mixed
     */
    static function show()
    {
        return DB::table('tbl_currency_details')
        		->select('name', 'code', 'rate')
        		->where('status', 1)
        		->get();
    }
}

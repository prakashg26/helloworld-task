<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerObjects();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register custom objects and singleton functions.
     */
    private function registerObjects()
    {
        /**
         * Macro for response
         */
        $this
            ->app
            ->make(ResponseFactory::class)
            ->macro('jsend', function ($data = null, $message = null, $code = 200, $status = 'success') {
                if ($message instanceof MessageBag) {
                    $message = $message->first();
                }

                if ($code >= 200 && $code < 300) {
                    $status = 'success';
                } elseif ($code >= 400 && $code < 500) {
                    $status = 'fail';
                } elseif ($code >= 500) {
                    $status = 'error';
                }

                return $this->json([
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                ], $code);
            });
    }
}
